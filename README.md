# Mini talk on Functional Patterns for Scala Applications 

This presentation is heavily based on the excellent talk 
given by Noel Markham at Lambda Days 2015.

This is a quick and "practical" presentation of few examples 
of functional patterns in Scala. It covers a way to abstract over
configuration within a modern Scala application; examples use Reader, ReaderT 
and Writer monads. Finally a technique to help functional testing is
presented.

Also, it's the perfect excuse to try REPLesent :)
 
My goal here was to 1) be sure to understand Noel's talk properly and 2) present
the topics to a few colleagues within our weekly workshops.

To run the presentation, open a `console` inside `sbt`.

Run the following line:

```
val rp = REPLesent(input="presentation.txt", intp=$intp); import rp._
```

Then open the first slide by typing `f`.

The next slide, or portion of the slide by typing `n`. Go back with `p`.

Run the code with `!!` or `r`.

For the original talk, see https://github.com/noelmarkham/scaladays-2015

For more info on REPLesent, see https://github.com/marconilanna/REPLesent