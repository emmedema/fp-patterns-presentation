package com.emmedema.workshop.external

import scala.concurrent.Future

import scalaz._
import Scalaz._
import argonaut._
import Argonaut._
import scala.concurrent.ExecutionContext.Implicits.global

object TradeSource {

  def getTrades(ids: List[String], env: String, user: String, pwd: String): Future[List[Trade]] = Future {
    val source = scala.io.Source.fromFile(s"trades_${env.toLowerCase}.json").mkString
    val trades = Parse.decodeOption[List[Trade]](source).getOrElse(throw new RuntimeException("Cannot parse trades file"))
    trades filter { t =>
      ids.contains(t.id)
    }
  }

  implicit def tradeCodec: CodecJson[Trade] = casecodec4(Trade.apply, Trade.unapply)("tradeRef", "instrument", "qty", "price")
  case class Trade(id: String, instrument: String, notional: Int, price: Double)

}

object Statistics {

  def calculateWeightedPrices(pricesByInstrument : List[(String,Int,Double)], ignorableInstruments : List[String]): Future[Map[String, BigDecimal]] = Future {
    def weightedAverage(dataPoints : List[(Int,BigDecimal)]) : BigDecimal = {
      val weights = dataPoints.map(_._1)
      val products = dataPoints.map{ case (notional, price) => notional * price}

      products.sum / weights.sum
    }

    val weightedPricesByInstrument = pricesByInstrument.groupBy(_._1).map {
      case (instrument, listOfTuples) =>
        (instrument, listOfTuples.map{
          case (i,n,p) => (n,BigDecimal(p))
        })
    }.map {
      case (instrument, listOfTuples) =>
        (instrument, weightedAverage(listOfTuples))
    }

    weightedPricesByInstrument.filterNot{
      case (instrument,_) => ignorableInstruments.contains(instrument)
    }
  }

}
