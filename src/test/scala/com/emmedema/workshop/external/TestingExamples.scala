package com.emmedema.workshop.external

import com.emmedema.workshop.external.TradeSource.Trade
import org.scalacheck.Prop.forAll
import shapeless.contrib.scalacheck._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scalaz.Kleisli._
import scalaz.Scalaz._
import scalaz._

class TestingExamples extends org.scalatest.FunSuite {

  val env = "env"
  val apiUser = "matteo"
  val apiPwd = "myPwd"
  val ignorable = List("GOOG")

  case class Config(env: String, user: String, pwd: String, ignorable: List[String])

  test("Step 1: Wiring the methods together") {

    def weightedPrices(ids: List[String]): Future[Map[String, BigDecimal]] =

      for {
        trades <- TradeSource.getTrades(ids, env, apiUser, apiPwd)
        listOfTrades = trades.map { case Trade(_, i, n, p) => (i, n, p) }
        r <- Statistics.calculateWeightedPrices(listOfTrades, ignorable)
      } yield r

  }

  test("Step 2: Extract the configuration") {


    import Kleisli._

    def getTrades(ids: List[String]): ReaderT[Future, Config, List[Trade]] = {
      kleisli { c =>
        TradeSource.getTrades(ids, c.env, c.user, c.pwd)
      }
    }

    def calculateWeightedPrices(pricesByInstrument: List[(String, Int, Double)])
                            : ReaderT[Future, Config, Map[String, BigDecimal]] = {
      kleisli { c =>
        Statistics.calculateWeightedPrices(pricesByInstrument, c.ignorable)
      }
    }

    def weightedPrices(ids: List[String]): ReaderT[Future, Config, Map[String, BigDecimal]] = {

      for {
        trades <- getTrades(ids)
        listOfTrades = trades.map { case Trade(_, i, n, p) => (i, n, p) }
        r <- calculateWeightedPrices(listOfTrades)
      } yield r

    }
  }

  test("Step 3: Testing") {

    def weightedPrices(
           getTradesF : List[String] => ReaderT[Future, Config, List[Trade]],
           calculateWeightedPricesF : List[(String, Int, Double)] => ReaderT[Future, Config, Map[String, BigDecimal]]
    )(ids: List[String]): ReaderT[Future, Config, Map[String, BigDecimal]] = {
      for {
        trades <- getTradesF(ids)
        listOfTrades = trades.map{case Trade(_,i,n,p)=> (i,n,p)}
        r <- calculateWeightedPricesF(listOfTrades)
      } yield r
    }

    val prop = forAll { (trades: List[Trade]) =>
      def tradesR(ids: List[String]) : ReaderT[Future, Config, List[Trade]] = {
        kleisli { _ => Future(trades) }
      }

      def wpR(wps : List[(String, Int, Double)]): ReaderT[Future, Config, Map[String, BigDecimal]] = {
        kleisli { _ => Future(Map("AAPL" -> 11.125)) }
      }

      val r = weightedPrices(tradesR, wpR)(List())
      val receivedMap = Await.result( r.run(Config("env", "user", "pwd", List())), 1.second )

      Map("AAPL" -> 11.125) === receivedMap
    }
  }

  test("Step 4: Abstraction") {
    def weightedPrices[M[_] : Monad](
            getTradesF : List[String] => M[List[Trade]],
            calculateWeightedPricesF : List[(String,Int,Double)] => M[Map[String, BigDecimal]])
      (ids: List[String]): M[Map[String, BigDecimal]] = {

      for {
        trades <- getTradesF(ids)
        listOfTrades = trades.map{case Trade(_,i,n,p)=> (i,n,p)}
        r <- calculateWeightedPricesF(listOfTrades)
      } yield r

    }

    val prop = forAll { (trades: List[Trade]) =>

      def tradesR(ids: List[String]): Id[List[Trade]] = trades
      def wpR(wps : List[(String,Int,Double)]) : Id[Map[String, BigDecimal]] = Map("AAPL" -> 11.125)

      val r = weightedPrices(tradesR, wpR)(List())

      Map("AAPL" -> 11.125) === r

    }
  }

  test("Step 5: Writer Monad") {

    def weightedPrices[M[_] : Monad](
          getTradesF: List[String] => M[List[Trade]],
          calculateWeightedPricesF: List[(String,Int,Double)] => M[Map[String, BigDecimal]],
          logF: String => M[Unit])(ids: List[String]): M[Map[String, BigDecimal]] = {

      for {
        trades <- getTradesF(ids)
        _ <- logF(s"Obtained trades: $trades")
        listOfTrades = trades.map{case Trade(_,i,n,p)=> (i,n,p)}
        r <- calculateWeightedPricesF(listOfTrades)
        _ <- logF(s"Calculated prices: $r")
      } yield r

    }

    type W[X] = Writer[Map[String, Int], X] // brevity here

    val prop = forAll { (trades: List[Trade]) =>

      def tradesR(ids: List[String]): W[List[Trade]] = trades.set(Map("trades" -> 1))
      def wpR(wps : List[(String,Int,Double)]) : W[Map[String, BigDecimal]] = Map("AAPL" -> BigDecimal(11.125)).set(Map("wp" -> 1))
      def logF(text: String): W[Unit] = Map("log" -> 1).tell

      val run = weightedPrices(tradesR, wpR, logF)(List()).run

      // convertToEqualizer() here is necessary to shadow ambiguous implicit conversions
      convertToEqualizer(Map("trades" -> 1, "wp" -> 1, "log" -> 2)) === run._1
      convertToEqualizer(Map("AAPL" -> 11.125)) === run._2
    }
  }
}
